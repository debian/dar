Source: dar
Section: utils
Priority: optional
Maintainer: John Goerzen <jgoerzen@complete.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13), pkgconf, zlib1g-dev, libbz2-dev,
 libzstd-dev, liblzo2-dev, liblzma-dev, liblz4-dev, libext2fs-dev,
 libgcrypt20-dev, libgpgme-dev, libassuan-dev, libargon2-dev,
 librsync-dev, libcap-dev, libnsl-dev,
 doxygen, groff, libthreadar-dev (>= 1.5.0),
 libcurl4-openssl-dev, libssl-dev,
 python3-dev, dh-python, python3-setuptools, python3-pybind11
Build-Conflicts: libcurl4-gnutls-dev, graphviz
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: http://dar.linux.free.fr/
Vcs-Browser: https://salsa.debian.org/debian/dar
Vcs-Git: https://salsa.debian.org/debian/dar.git

Package: libdar-dev
Section: libdevel
Architecture: any
Depends: libdar64-6000t64 (= ${binary:Version}), ${misc:Depends}, zlib1g-dev,
 libbz2-dev, libzstd-dev, liblzo2-dev, liblzma-dev, liblz4-dev,
 libgcrypt20-dev, libgpg-error-dev, libassuan-dev, libargon2-dev
Description: Disk ARchive: Development files for shared library
 Full featured archiver with support for differential backups, slices,
 compression, ATTR/ACL support. DAR also supports Pipes for remote
 operations, including with ssh.

Package: python3-dar
Architecture: any
Section: python
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: Disk ARchive: Python bindings
 Full featured archiver with support for differential backups, slices,
 compression, ATTR/ACL support. DAR also supports Pipes for remote
 operations, including with ssh.

Package: libdar64-6000t64
Provides: ${t64:Provides}
Replaces: libdar64-6000
Breaks: libdar64-6000 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Disk ARchive: Shared library
 Full featured archiver with support for differential backups, slices,
 compression, ATTR/ACL support. DAR also supports Pipes for remote
 operations, including with ssh.
 .
 This package contains the shared library for accessing archives.

Package: dar-static
Architecture: any
Depends: ${misc:Depends}
Built-Using: ${Built-Using}
Description: Disk ARchive: Backup directory tree and files
 Full featured archiver with support for differential backups, slices,
 compression, ATTR/ACL support. DAR also supports Pipes for remote
 operations, including with ssh.
 .
 This package contains the static binary, dar_static.

Package: dar
Architecture: any
Depends: libdar64-6000t64 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Suggests: par2, dar-docs
Description: Disk ARchive: Backup directory tree and files
 Full featured archiver with support for differential backups, slices,
 compression, ATTR/ACL support. DAR also supports Pipes for remote
 operations, including with ssh.

Package: dar-docs
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Disk ARchive: Backup directory tree and files
 Full featured archiver with support for differential backups, slices,
 compression, ATTR/ACL support. DAR also supports Pipes for remote
 operations, including with ssh.
 .
 This package contains the documentation and example files.
