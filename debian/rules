#!/usr/bin/make -f
# -*- makefile -*-


# The multi-build in this file is based on the example
# in kmod.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DEB_CONFIGURE_EXTRA_FLAGS := --disable-upx \
			     --enable-mode=64
DEB_CONFIGURE_EXTRA_FLAGS += --libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH)

BUILT_USING_PACKAGES = libc-dev-bin libbz2-dev libgcrypt20-dev libgpgme-dev liblzo2-dev zlib1g-dev libzstd-dev liblz4-dev libargon2-dev libassuan-dev librsync-dev libext2fs-dev libcap-dev libnsl-dev libthreadar-dev libssl-dev libcurl4-openssl-dev

BUILT_USING=$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W $(BUILT_USING_PACKAGES))

override_dh_clean:
	rm -rf build-*
	rm -rf debian/tmp*
	dh_clean

build-main/config.status: configure
	dh_auto_configure --builddir=build-main -- $(DEB_CONFIGURE_EXTRA_FLAGS) --disable-dar-static --enable-python-binding

build-static/config.status: configure
	dh_auto_configure --builddir=build-static -- \
		$(DEB_CONFIGURE_EXTRA_FLAGS) --disable-shared --disable-libcurl-linking --disable-python-binding

build-main/.stamp-build: build-main/config.status
	dh_auto_build --builddir=build-main
	touch $@

build-static/.stamp-build: build-static/config.status
	find . -name Makefile.in -exec sed -i 's/@LIBS@/@LIBS@ -lassuan/g' {} \;
	dh_auto_build --builddir=build-static
	touch $@

override_dh_auto_build: build-main/.stamp-build build-static/.stamp-build

override_dh_auto_configure:
	# Don't do the normal steps here due to separate builds

override_dh_auto_test:
	echo testing is no op

override_dh_auto_install:
	dh_auto_install --builddir=build-static --destdir=debian/tmp-static
	dh_auto_install --builddir=build-main
	mv $(CURDIR)/debian/tmp/usr/share/dar/samples \
		$(CURDIR)/debian/tmp/usr/share/examples
	chmod a-x $(CURDIR)/debian/tmp/usr/share/examples/MyBackup.sh.tar.gz
	mkdir -p $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages
	mv $(CURDIR)/debian/tmp/usr/lib/*/python3/dist-packages/* \
		$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/
	cp -a $(CURDIR)/debian/tmp-static/usr/bin/*_static \
		$(CURDIR)/debian/tmp/usr/bin/
	cp $(CURDIR)/debian/tmp/usr/share/man/man1/dar.1 \
		$(CURDIR)/debian/tmp/usr/share/man/man1/dar_static.1

override_dh_gencontrol:
	dh_gencontrol
	dh_gencontrol -pdar-static -- -VBuilt-Using="$(BUILT_USING)"

%:
	dh $@ --with python3

.PHONY: override_dh_auto_configure override_dh_auto_test \
	override_dh_auto_install override_dpkg-gencontrol
